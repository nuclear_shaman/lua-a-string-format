--[[---------------------------------------------------------------------------
    --TODO: empty {} symbols need to be counted
    --TODO: create tests with examples
    --TODO: improve the parsing alg

    Format syntax:
    {format:?n.field,conversion}

    Function call example:
    call: aprint("test {} number {2}","call",1)
    output: test call number 1

    {} - empty symbol, replaced by an argumment currently iterated on, the
    number of empty symbols needs to be the same as the number of arguments.
    {n} - numbered symbol, it is replaced by the argument of the index n
    where n is a number.
    {n.myField} - the symbol is replaced by the field "myField" of the argument
    of number n
    {n.myField,b} - the symbol is replaced by the field "myField" of the argument
    with index n written in binary format.
    {?n} - the symbol is replaced with argument of index n printed in table mode,
    in case the argument is a table the table contents are also printed. In case
    the argument is not a table, the contents are printed normally.
    {0-5:} - the symbol is printed in 5 characters, if the symbol is shorter than
    5 characters the empty space is filled with zeros. The format is:
    fillCharacter-numberOfCharacters

    * Currently the {{}} multiple symbols are not supported.

    aformat was inspired by python string library (cpython/string.py)
--]]---------------------------------------------------------------------------

require "aconversion"

--[[
    
--]]
local format_field = function(value, format)
    if not format then
        return value
    else
        local formatWidth = nil
        local fillchar = nil
        local separatorChar = "-"

        fillchar = string.sub(format, 1, 1)

        if string.sub(format, 2, 2) ~= separatorChar then
            error("Incorrent fillchar separator format!")
        end

        formatWidth = tonumber(string.sub(format, 3))

        if not formatWidth then
            error("Incorrect format width format!")
        end

        if #tostring(value) < formatWidth then
            return string.rep(fillchar,formatWidth - #tostring(value)) .. value
        elseif #value == formatWidth then
            return value
        else
            return string.sub(value, formatWidth)
        end
    end
end


--[[
    
--]]
local convert_field = function(value, conversion)
    if not conversion then
        return value
    else
        if conversion == 'b' then --binary
            return toBinary(tonumber(value))
        elseif conversion == 'x' then --hexadecimal
            return toHexadecimal(tonumber(value))
        elseif conversion == 'X' then --hexadecimal uppercase
            return toHexadecimal(tonumber(value),true)
        elseif conversion == 'o' then --octal
            return toOctal(tonumber(value))
        else
            error("Invalid conversion!")
        end
    end
end


--[[
    _parse_field_name

    This function parses the field_name and returns the extra options needed
    to correctly print the object if there are any.

        - field_name:   the field name that gets parsed

    Return values
        - format:
        - tablePrint:
        - field:
        - conversion:
        - argIndex:

    TODO: obviously bad function, optimize...
--]]
local _parse_field_name = function(field_name)
    local format, tablePrint, field, conversion, argIndex = nil, nil, nil, nil, nil

    local STATE_START = 0
    local STATE_FORMAT_DONE = 1
    local STATE_DEBUG_DONE = 2
    local STATE_INDEX_DONE = 3
    local STATE_FIELD_START = 4
    local STATE_CONVERSION_START = 5

    local state = STATE_START
    local cursor = 1

    --This is the most often case, optimize for it.
    if tonumber(field_name) then
        return format, tablePrint, field, conversion, tonumber(field_name)
    end

    for i = 1, #field_name, 1 do
        local char = string.sub(field_name, i, i)

        if state == STATE_START then
            if char == ":" then
                format = string.sub(field_name,cursor,i-1)
                state = STATE_FORMAT_DONE
                cursor = i
            end
        end

        if state == STATE_FORMAT_DONE or state == STATE_START then
            if char == "?" then
                tablePrint = true
                state = STATE_DEBUG_DONE
                cursor = i
            end
        end

        if state == STATE_DEBUG_DONE or state == STATE_FORMAT_DONE or state == STATE_START then
            local tmpArgIndex = nil

            if state == STATE_START then
                tmpArgIndex = tonumber(string.sub(field_name, cursor, i - 1))
            else
                tmpArgIndex = tonumber(string.sub(field_name,cursor + 1, i - 1))
            end

            if char == "." then
                argIndex = tmpArgIndex
                state = STATE_FIELD_START
                cursor = i
            elseif char == "," then
                argIndex = tmpArgIndex
                state = STATE_CONVERSION_START
                cursor = i
            elseif i == #field_name then
                argIndex = tonumber(string.sub(field_name, cursor + 1))
            end
        end

        if state == STATE_FIELD_START then
            if char == "," then
                field = string.sub(field_name, cursor + 1, i - 1)
                state = STATE_CONVERSION_START
                cursor = i
            elseif i == #field_name then
                field = string.sub(field_name, cursor + 1)
            end
        end

        if state == STATE_CONVERSION_START then
            if i == #field_name then
                conversion = string.sub(field_name, cursor + 1)
            end
        end
    end

    return format, tablePrint, field, conversion, argIndex
end

--[[
    _aparse(form_str)

    This function returns a list of symbol objects that represents instructions
    what to replace those symbols with.
    The fields inside those symbol objects are:
        - literal_text: this represents literal text, and is nil in case the
                        symbol is not literal text
        - field_name:   this field is the symbol name, it can be a number that
                        coresponds to the variable in the argument list of the
                        format function, or a number with a access operator
                        (1.field) that represents the acess operator done on the
                        argument from the list.
                        In case the value is "" the symbol was non indexed.
--]]
local _aparse = function(form_str)
    local STATE_CURLY_NONE   = 1
    local STATE_CURLY_FOUND  = 2
    local lookUpState = STATE_CURLY_NONE

    local result = {}
    local symSplit = {}

    local curlOpenLoc = {}
    local curlCloseLoc = {}

    local cursor = 1

    for i = 1, #form_str, 1 do
        local char = string.sub(form_str, i, i)
        if lookUpState == STATE_CURLY_NONE then
            if char == "{" and string.sub(form_str, i - 1, i - 1) ~= "\\" then
                lookUpState = STATE_CURLY_FOUND
                table.insert(curlOpenLoc, i)
                if i > 1 then
                    table.insert(symSplit,string.sub(form_str, cursor, i - 1))
                    cursor = i
                end
            elseif i == #form_str then
                table.insert(symSplit, string.sub(form_str, cursor))
            end
        elseif lookUpState == STATE_CURLY_FOUND then
            if char == "}" and form_str[i - 1] ~= "\\" then
                lookUpState = STATE_CURLY_NONE
                table.insert(curlCloseLoc, i)
                table.insert(symSplit,string.sub(form_str, cursor, i))
                cursor = i + 1
            end
        end
    end

    if #curlCloseLoc ~= #curlOpenLoc  then
        error("A-FORMAT error: curly braces left unclosed!!")
    end

    for _, sym in ipairs(symSplit) do
        if string.sub(sym, 1, 1) ~= "{" then
            table.insert(result, {["literal_text"] = sym})
        elseif sym == "{}" then
            table.insert(result, {["field_name"] = ""})
        else
            table.insert(result, {["field_name"] = string.sub(sym, 2, #sym - 1)})
        end
    end

    return result
end

--[[
    get_value

    This function gets the object out of argument table.
    If the object does not exsist, it throws an error.
--]]
local get_value = function(argIndex, field, args)
    result = args[argIndex]

    if not result then
        error("Index out of bounds!")
    end

    if field then
        result = result[field]

        if not result then
            error("Table field does not exsist!")
        end
    end

    return result
end

--[[

    auto_arg_index -- number of automatic variable (the ones inside {})
--]]
local _aformat = function(form_str, args)
    local result = {}
    local auto_arg_index = 1

    for _, sym in pairs(_aparse(form_str)) do --TODO: _APARSE!!
        local literal_text = sym.literal_text
        local field_name = sym.field_name

        if literal_text then
            table.insert(result, literal_text)
        end

        if field_name then
            if field_name == "" then
                field_name = tostring(auto_arg_index)
                auto_arg_index = auto_arg_index + 1
            end

            -- given the field name find the object it references
            local format, tablePrint, field, conversion, argIndex = _parse_field_name(field_name)

            -- get the object value from argument list
            obj = get_value(argIndex, field, args)

            if tablePrint then
                if conversion or format then
                    error("Conversion or formatting of tables in table print mode is not supported")
                end

                obj = atableToStr(obj)
            else
                -- do conversions on objects
                obj = convert_field(obj, conversion)

                -- do formatting on objects
                obj = format_field(obj, format)
            end

            table.insert(result, tostring(obj))
        end
    end

    return table.concat(result)
end


--[[---------------------------------------------------------------------------
    PUBLIC CODE
--]]---------------------------------------------------------------------------


--[[
    aprint

    Advanced print function
--]]
function aprint(form_str, ...)
    print(aformat(form_str, ...))
end

--[[
    aformat

    Advanced format function
--]]
function aformat (form_str, ...)
    result = _aformat(form_str, arg)

    return result
end

--[[
    
--]]
function atableToStr(myTable, recDepth)
    local result = {}
    local cnt = 1
    MAXIMUM_RECURSION_DEPTH = 4
    recDepth = recDepth or 0

    table.insert(result, "{")
    for key, value in pairs(myTable) do
        --[[if type(key) ~= "number" then
            table.insert(result,key)
            table.insert(result,"=")
        end]]

        if type(value) == "table" and recDepth < MAXIMUM_RECURSION_DEPTH then
            table.insert(result, atableToStr(value, recDepth + 1))
        else
            table.insert(result, tostring(value))
        end

        if cnt ~= #myTable then
            cnt = cnt + 1
            table.insert(result, ", ")
        end
    end
    table.insert(result, "}")

    return table.concat(result)
end