
function toBinary(num)
    local result = {}

    while num > 0 do
        rest = math.fmod(num, 2)
        result[#result + 1] = rest
        num = (num - rest) / 2
    end

    return table.concat(result)
end

function toHexadecimal(num,upper)
    local result = {}
    local hexCharset = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"}

    repeat
        table.insert(result, 1, hexCharset[num % 16 + 1])
        num = math.floor(num / 16)
    until num == 0

    if upper then
        return string.upper(table.concat(result))
    else
        return table.concat(result)
    end
end

function toOctal(num)
    local result = {}
    local octCharset = {"0","1","2","3","4","5","6","7"}

    repeat
        table.insert(result, 1, octCharset[num % 8 + 1])
        num = math.floor(num / 8)
    until num == 0

    return table.concat(result)
end