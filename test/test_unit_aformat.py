import pexpect
import re
import pytest

def invoke_luaLine(luaFile, luaCode):
    child = pexpect.spawn('lua -i -l ' + luaFile)
    child.readline()
    child.sendline(luaCode)
    child.readline()
    retval = child.readline()[:-2].decode('ascii')
    child.close()

    return retval

## TESTS

def test_unordered():
    # Arrange
    luaTestCode = 'aprint("first 3 prime {}: {}, {}, {}", "numbers", 2, 3, 7)'
    expectedOutput = "first 3 prime numbers: 2, 3, 7"
    # Act
    actualOutput = invoke_luaLine('aformat',luaTestCode)
    # Assert
    assert expectedOutput == actualOutput

def test_enumerated():
    # Arrange
    luaTestCode = 'aprint("first 3 prime {4}: {3}, {2}, {1}", 7, 3, 2, "numbers")'
    expectedOutput = "first 3 prime numbers: 2, 3, 7"
    # Act
    actualOutput = invoke_luaLine('aformat',luaTestCode)
    # Assert
    assert expectedOutput == actualOutput

def test_tablePrint():
    # Arrange
    luaTestCode = 'aprint("{1} printout: {?1}", {"field 1", 1, 2, {"inner table", 2, 3}})'
    expectedOutput = "table: 0x[0-9A-Fa-f]+ printout: {field 1, 1, 2, {inner table, 2, 3}}"
    # Act
    actualOutput = invoke_luaLine('aformat',luaTestCode)
    # Assert
    assert re.match(expectedOutput,actualOutput)

def test_hexPrint():
    # Arrange
    luaTestCode = 'aprint("the {1} {2} can be represented in hex format both in uppercase and lowercase: {2,x}/{2,X}", "number", 203)'
    expectedOutput = 'the number 203 can be represented in hex format both in uppercase and lowercase: cb/CB'
    # Act
    actualOutput = invoke_luaLine('aformat',luaTestCode)
    # Assert
    assert expectedOutput == actualOutput

def test_binaryPrint():
    # Arrange
    luaTestCode = 'aprint("the number {1} in binary: {1,b}", 203)'
    expectedOutput = 'the number 203 in binary: 11010011'
    # Act
    actualOutput = invoke_luaLine('aformat', luaTestCode)
    # Assert
    assert expectedOutput == actualOutput

@pytest.mark.parametrize(
    "num, binNum",
    [
        ('1','00000001'),
        ('2','00000010'),
        ('3','00000011'),
        ('4','00000100'),
        ('5','00000101'),
        ('6','00000110'),
        ('7','00000111'),
        ('8','00001000'),
        ('9','00001001'),
        ('64','01000000'),
        ('65','01000001'),
        ('128','10000000'),
        ('129','10000001'),
        ('255','11111111'),
    ]
)
def test_binaryPrintInfill(num, binNum):
    # Arrange
    luaTestCode = 'aprint("number {1} in binary representation, written with 8 digits:     {0-8:1,b}", ' + num + ')'
    expectedOutput = 'number ' + num + ' in binary representation, written with 8 digits:     ' + binNum
    # Act
    actualOutput = invoke_luaLine('aformat', luaTestCode)    
    # Assert
    assert expectedOutput == actualOutput

@pytest.mark.parametrize(
    "num, infillChar, infillNum, expected",
    [
        ('5', '*', '8', '*******5'),
        ('15', '*', '8', '******15'),
        ('105', '+', '8', '+++++105'),
        ('368863', '-', '10', '----368863'),
        ('1', 'A', '2', 'A1'),
        ('1', ' ', '4', '   1'),
    ]
)
def test_decimalPrintInfill(num, infillChar, infillNum, expected):
    # Arrange
    luaTestCode = 'aprint("number {1} in decimal representation infilled with '+ infillChar +' character: {'+ infillChar +'-'+ infillNum +':1}", ' + num + ')'
    expectedOutput = 'number '+ num +' in decimal representation infilled with '+ infillChar +' character: ' + expected
    # Act
    actualOutput = invoke_luaLine('aformat', luaTestCode)
    # Assert
    assert expectedOutput == actualOutput

def test_octalPrintInfill():
    # Arrange
    luaTestCode = 'aprint("number {1} in octal representation infilled with white spaces:  { -8:1}", 8)'
    expectedOutput = 'number 8 in octal representation infilled with white spaces:         8'
    # Act
    actualOutput = invoke_luaLine('aformat', luaTestCode)
    # Assert
    assert expectedOutput == actualOutput
